import React, { Component } from "react";
import {
  Carousel
} from 'react-bootstrap';
import "./Home.css";
import pic1 from "../pictures/hero1.jpg";
import pic2 from "../pictures/hero2.JPG";
import pic3 from "../pictures/hero3.JPG";

export default class Home extends Component {
  render() {
    return (
      <div className="Home">
      <Carousel interval="10000">
        <Carousel.Item align="middle">
          <img width={900} height={500}  alt="Rocket" src={pic1} />
          <Carousel.Caption>
            <h3>Rocket!</h3>
            <p>This is a rocket</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item align="middle">
          <img width={900} height={500} alt="900x500" src={pic2} />
          <Carousel.Caption>
            <h3>Rocket Launch</h3>
            <p>Yay! Takeoff!</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item align="middle">
          <img width={900} height={500} alt="900x500" src={pic3}/>
          <Carousel.Caption>
            <h3>Simulation</h3>
            <p>Whoa pretty</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item align="middle">
          <img width={900} height={500} alt="900x500" src="https://i.imgur.com/YHqMjwR.gif"/>
          <Carousel.Caption>
            <h3>Simulation</h3>
            <p>Whoa pretty</p>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>
      <h1 className='primary-home-title'>Rocketry@VT</h1>
        <h3 className='secondary-home-title'>An undergraduate high-powered rocket engineering design team</h3>
        <p className='home-intro'>
        Rocketry at Virginia Tech is an organization dedicated to exploring and testing the limits
        of high power amateur rocketry, and sharing that experience with the rest of Virginia Tech.
        As a club, we help students get their high power rocketry certification from Tripoli Rocket
        Association or the National Association of Rocketry, with the support of New River Valley
        Rocketry, the local rocket club. As a design team, we compete in competitions across the
        country, continually pushing the limits of what we know to go higher, faster, and farther.
        </p>
      </div>
    );
  }
}
