import React, { Component } from "react";
import "./Team.css";
import Data from "../data/data.json"; 

export default class Team extends Component {
  render() {
    return (
        <div>
        <h2>Team Leads</h2>
        <hr />
        <ul className='teamlist'>
            {Data.leads.map((person) =>
            <ul className='space-list-items'>
                <li><img className='avatar' src={person.picture} alt="cat"/></li>
                <li className="lead-name">{person.name}</li>
                <li className="role">{person.role}</li>
            </ul>
            )}
        </ul>
        <h3>Team Members</h3>
        <hr />
        <ul className='teamlist'>
            {Data.members.map((person) =>
            <ul className='space-list-items'>
                <li><img className='avatar' src={person.picture} alt="cat"/></li>
                <li>{person.name}</li>
            </ul>
            )}
        </ul>
    </div>
    );
  }
}
