import React from "react";
import { Route, Switch } from "react-router-dom";
import Home from "./containers/Home";
import NotFound from "./containers/NotFound";
import About from "./containers/About";
import Apply from "./containers/Apply";
import Team from "./containers/Team";

export default () =>
  <Switch>
    <Route path="/" exact component={Home} />
    <Route path="/about" exact component={About} />
    <Route path="/apply" exact component={Apply} />
    <Route path="/team" exact component={Team} />
    <Route component={NotFound} />
  </Switch>;