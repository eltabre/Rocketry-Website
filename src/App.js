import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Navbar, Nav} from "react-bootstrap";
import "./App.css";
import Routes from "./Routes";
import RouteNavItem from "./components/RouteNavItem";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Navbar fluid inverse collapseOnSelect style={{marginBottom: "0"}}>
          <Navbar.Header>
            <Navbar.Brand>
              <Link to="/">Rocketry@VT Under Development</Link>
            </Navbar.Brand>
            <Navbar.Toggle />
          </Navbar.Header>
          <Navbar.Collapse>
          <Nav>
            <RouteNavItem  href="/about">About</RouteNavItem >
            <RouteNavItem  href="/team">Team</RouteNavItem >
          </Nav>
          <Nav pullRight>
            <RouteNavItem  pullright href="/apply">Apply Now</RouteNavItem >
          </Nav>
        </Navbar.Collapse>
        </Navbar>
        <Routes />
      </div>
    );
  }
}

export default App;
