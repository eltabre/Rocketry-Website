import React from "react";
import "./NotFound.css";

export default () =>
  <div className="NotFound">
    <h3>Sorry, the page you are looking for is somewhere in orbit!</h3>
  </div>;