import React, { Component } from "react";
import {Grid, Col, Row} from 'react-bootstrap'; 
import "./About.css";
import pic1 from "../pictures/rocketstand.JPG";

export default class About extends Component {
  render() {
    return (
      <div className="About">
        <div>
          <h1>About</h1>
          <h3>IREC 2017</h3>
          <Grid>
              <Row>
                  <Col className="about-text" xs={6} md={6} lg={6}>
                  <p>This past summer, we competed in the Intercollegiate Rocket Engineering
                      Competition (IREC), hosted by the Experimental Sounding Rocket Association.
                      We entered and competed in the 10,000 commercial off the shelf category.
                      The competition was held at Spaceport America in Las Cruces, NM.
                      We participated with 115 teams from 6 continents in arguably the most
                      prestigious rocket competition in the world.</p>
                  </ Col>
                  <Col  xs={6} md={6} lg={6}>
                  <img src={pic1} alt="rocket" className="about-image"/>
                  </ Col>
              </Row>
          </Grid>
        </div>
      </div>
    );
  }
}